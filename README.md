# Forkio
   You can watch it [here](https://daniil_khlyvniuk.gitlab.io/step-2)

## Technologies:
- HTML 5
- Scss, css3
- JavaScript ES6
- npm 
- Gulp
- Git 
- [GitLab pages](https://daniil_khlyvniuk.gitlab.io/step-2)

### Authors:
##### Oleksii Lubianyi
   - Sections: "header", "People Are Talking About Fork"

##### Daniil Khlyvniuk
   - Sections: "Revolutionary Editor", "Here is what you get", "Fork Subscription Pricing"

#### Commands to build: 
- dev: "npm start"
- production: "npm run prod"    