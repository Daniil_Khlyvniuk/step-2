"use strict";

const menuBtn = document.querySelector(".menu__btn");
const boxMenu = document.querySelector(".menu__box ");

function showMenu() {
  boxMenu.classList.toggle("show");
  document.querySelector(".menu__btn span").classList.toggle("active");
}
menuBtn.addEventListener("click", showMenu);

window.addEventListener("click", (e) => {
  const target = e.target;
  if (!target.closest(".menu__box") && !target.closest(".menu__btn")) {
    boxMenu.classList.remove("show");
    document.querySelector(".menu__btn span").classList.remove("active");
  }
});
